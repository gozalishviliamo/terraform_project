region = "eu-central-1"

subnet = "10.0.30.0/24"

azones = "eu-central-1c"

ingress_rules = [ 22,80,443 ]
egress_rules = [ 22,80,443 ]

instance_type = "t2.micro"
ec2_name = "testing instance"

web_traffic_name = "testing web security"

elastic_ip_name = "testing ip address"

key_name = "aws_test_key_pair"
