region = "eu-central-1"

subnet = "10.0.20.0/24"

azones = "eu-central-1b"

ingress_rules = [ 22,80,443 ]
egress_rules = [ 22,80,443 ]

instance_type = "t2.micro"
ec2_name = "staging instance"

web_traffic_name = "staging web security"

elastic_ip_name = "staging ip address"

key_name = "aws_stage_key_pair"

