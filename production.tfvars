region = "eu-central-1"

subnet = "10.0.10.0/24"

azones = "eu-central-1a"

ingress_rules = [ 22,80,443 ]
egress_rules = [ 22,80,443 ]

instance_type = "t2.micro"
ec2_name = "production instance"

web_traffic_name = "production web security"

elastic_ip_name = "Production ip address"

key_name = "aws_prod_key_pair"
